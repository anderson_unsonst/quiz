<script src="https://code.jquery.com/jquery-3.3.1.min.js"
              integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
              crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script type="text/javascript">
  $(document).ready(function()
    {
     $('#conferir').on('click', function(){
       $("li.errada").css("color", "red");
        $("li.respostas").css("color", "green");
      } );


     $('#atualiza').on('click', function() {
        location.reload(); 
     });

    });
</script>



<div class="container">
  
 
<div >
<h1>Quiz: Qual o nome desse país ?</h1>
 <img class="bandeira" src="{{$sorteado['normal']}}">
 <ul>
 @foreach($respostas as $resposta)

    @if($resposta == $certa)
    <li class="respostas"><input type="radio" name="pais" value="{{$resposta}}"> {{$resposta}}</li>
    @else
    <li class="errada"><input type="radio" name="pais" value="{{$resposta}}"> {{$resposta}}</li>
    @endif
  @endforeach
 </ul>

 <input type="button" name="enviar" value="Conferir Resposta" id="conferir"> 

  <input type="button" name="enviar" value="Proxima Quiz" id="atualiza"> 
</div>
   
</div> <!-- container-->
<!-- Modal -->
