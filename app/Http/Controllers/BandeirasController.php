<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BandeirasController extends Controller
{
    
    public function index(Request $request)
    {

		$path = storage_path() . "/json/paises.json";
		$json = json_decode(file_get_contents($path), true); 

	    $rand =	array_rand($json['paises'], 1);
	    $sorteado = $json['paises'][$rand];


	    $naosorteadorand =	array_rand($json['paises'], 3);
        $respostas = [$naosorteadorand[0], $naosorteadorand[1], $rand, $naosorteadorand[2]];

        shuffle($respostas);

    	return view('Bandeiras.index', ['sorteado'=>$sorteado, 'respostas'=>$respostas, 'certa'=>$rand]);
    }

    public function lista() {

    	return view('bandeiras.index');
    }
}
